package com.keta.generate.core;

import com.keta.generate.db.DataSource;
import com.keta.generate.db.Mysql;
import com.keta.generate.db.Oracle;
import com.keta.generate.util.ConvertHandler;
import com.keta.generate.util.Resources;
import com.keta.generate.vo.Table;

public class GenerateFactory
{
    private Table table;
    
    private String tableName;
    
    public GenerateFactory()
    {
        this.tableName = Resources.TPL_TABLE_NAME;
    }
    
    /**
     * 
     */
    public GenerateFactory(String tableName)
    {
        this.tableName = tableName;
    }
    
    public GenerateFactory(Table table)
    {
        this.table = table;
    }
    
    private void init()
        throws Exception
    {
        String dialect = Resources.JDBC_DRIVER;
        DataSource db;
        if ("com.mysql.jdbc.Driver".equals(dialect))
        {
            db = new Mysql();
        }
        else
        {
            db = new Oracle();
        }
        table = db.getTable(tableName);
        ConvertHandler.tableHandle(table);
    }
    
    /**
     * 生成java代码
     * 
     * @throws Exception
     * @see [类、类#方法、类#成员]
     */
    public void genJavaTemplate()
        throws Exception
    {
        if (table == null)
        {
            init();
        }
        new GenerateCode().generate(table);
    }
}
