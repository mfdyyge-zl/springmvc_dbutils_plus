package ${packageName}.core;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ${packageName}.common.DaoException;

/**
 * 
 * TransactionProvider 事务支持提供类
 * 
 * @author 00fly
 * @version [版本号, ${date?date}]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
public abstract class TransactionProvider
{
    private Logger log = LoggerFactory.getLogger(this.getClass());
    
    /**
     * 事务委托处理类
     */
    private TransactionManager transactionManager = MySqlDBUtil.getInstance().getTranManager();
    
    /**
     * 开启事务
     * 
     */
    public void startTransaction()
    {
        try
        {
            transactionManager.beginTransaction();
        }
        catch (DaoException e)
        {
            e.printStackTrace();
            log.error(e.getMessage());
        }
    }
    
    /**
     * 提交事务并关闭连接
     * 
     * @see [类、类#方法、类#成员]
     */
    public void commitAndClose()
    {
        try
        {
            transactionManager.commitAndClose();
        }
        catch (DaoException e)
        {
            e.printStackTrace();
            log.error(e.getMessage());
        }
    }
    
    /**
     * 回滚事务并关闭连接
     * 
     * @see [类、类#方法、类#成员]
     */
    public void rollbackAndClose()
    {
        try
        {
            transactionManager.rollbackAndClose();
        }
        catch (DaoException e)
        {
            e.printStackTrace();
            log.error(e.getMessage());
        }
    }
}
