package	${pknServiceImpl};

import java.util.List;

import org.junit.Assert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ${packageName}.common.DaoException;
import ${packageName}.common.PaginationSupport;
import ${packageName}.common.ServiceException;
import ${packageName}.core.TransactionProvider;
import ${pknDAO}.${className}DAO;
import ${pknDAO}.impl.${className}DAOImpl;
import ${pknEntity}.${className};
import ${pknService}.${className}Service;

/**
 * 
 * ${className}Service 接口实现类
 * 
 * @author 00fly
 * @version [版本号, ${date?date}]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
public class ${className}ServiceImpl extends TransactionProvider implements ${className}Service 
{
    private Logger log = LoggerFactory.getLogger(getClass());
    
	private ${className}DAO ${instanceName}DAO = new ${className}DAOImpl();
	
	
    @Override
    public void insert(${className} ${instanceName})
        throws ServiceException
    {
        try
        {
            ${instanceName}DAO.insert(${instanceName});
        }
        catch (DaoException e)
        {
            log.error(e.getMessage());
            throw new ServiceException(e.getMessage(), e.getCause());
        }
    }
    
    @Override
    public void deleteById(Long id)
        throws ServiceException
    {
        try
        {
            ${instanceName}DAO.deleteById(id);
        }
        catch (DaoException e)
        {
            log.error(e.getMessage());
            throw new ServiceException(e.getMessage(), e.getCause());
        }
    }
    
    @Override
    public long deleteById(Long[] ids)
        throws ServiceException
    {
        try
        {
            return ${instanceName}DAO.deleteById(ids);
        }
        catch (DaoException e)
        {
            log.error(e.getMessage());
            throw new ServiceException(e.getMessage(), e.getCause());
        }
    }
    
    @Override
    public long deleteById(List<Long> ids)
        throws ServiceException
    {
        try
        {
		    return ${instanceName}DAO.deleteById(ids);
        }
        catch (DaoException e)
        {
            log.error(e.getMessage());
            throw new ServiceException(e.getMessage(), e.getCause());
        }
    }
    
    @Override
    public void update(${className} ${instanceName})
        throws ServiceException
    {
        try
        {
            ${instanceName}DAO.updateById(${instanceName});
        }
        catch (DaoException e)
        {
            log.error(e.getMessage());
            throw new ServiceException(e.getMessage(), e.getCause());
        }
    } 
 
	@Override
	public void saveOrUpdate(${className} ${instanceName}) 
        throws ServiceException
    {
        try
        {
            if (${instanceName}.${pk.getMethod}() != null)
            {
                ${instanceName}DAO.updateById(${instanceName});
            }
            else
            {
                ${instanceName}DAO.insert(${instanceName});
            }
        }
        catch (DaoException e)
        {
            log.error(e.getMessage());
            throw new ServiceException(e.getMessage(), e.getCause());
        }
    }
    
    @Override
    public ${className} queryById(Long id)
        throws ServiceException
    {
        try
        {
		    return ${instanceName}DAO.queryById(id);
        }
        catch (DaoException e)
        {
            log.error(e.getMessage());
            throw new ServiceException(e.getMessage(), e.getCause());
        }
    }
    
    @Override
    public List<${className}> queryAll()
        throws ServiceException
    {
        try
        { 
            List<${className}> list = ${instanceName}DAO.queryAll(); 
            return list;
        }
        catch (DaoException e)
        { 
            log.error(e.getMessage());
            throw new ServiceException(e.getMessage(), e.getCause());
        }
    }
    
    
    /**
     * 根据条件分页查询
     * 
     * @param ${instanceName} 条件对象
     * @param pageNo 页号
     * @param pageSize 页大小
     * @return
     * @throws ServiceException
     */
    @Override
    public PaginationSupport<${className}> queryForPagination(${className} ${instanceName}, int pageNo, int pageSize)
        throws ServiceException
    {
        try
        {
            return ${instanceName}DAO.queryForPagination(${instanceName}, pageNo, pageSize);
        }
        catch (DaoException e)
        {
            log.error(e.getMessage());
            throw new ServiceException(e.getMessage(), e.getCause());
        }
    }
    
    /**
     * 事务方法
     * 
     * @throws ServiceException
     * @see [类、类#方法、类#成员]
     */
    public void testTrans()
        throws ServiceException
    {
        try
        {
            startTransaction();
            List<${className}> list = queryAll();
            for (${className} ${instanceName}: list)
            { 
                ${instanceName}DAO.insert(${instanceName});
                int i = 1 / 0; // 抛出异常
                Assert.assertNull(i);
            }
            commitAndClose();
        }
        catch (Exception e)
        {
            rollbackAndClose();
            log.error(e.getMessage());
            throw new ServiceException(e.getMessage(), e.getCause());
        }
    }
    
    /**
     * Main
     * 
     * @param args
     * @throws ServiceException
     * @see [类、类#方法、类#成员]
     */
    public static void main(String[] args)
        throws ServiceException
    {
        new ${className}ServiceImpl().testTrans();
    }
}
