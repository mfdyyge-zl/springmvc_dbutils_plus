package ${pknDAO};

import java.util.List;
import ${packageName}.common.DaoException;
import ${packageName}.common.PaginationSupport;
import ${pknEntity}.${className};

/**
 * 
 * ${className}DAO 接口
 * 
 * @author 00fly
 * @version [版本号, ${date?date}]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
public interface ${className}DAO
{   

    /**
     * 根据条件删除数据
     * 
     * @param criteria 条件对象
     * @return
     * @throws DaoException
     */ 
    int deleteByCriteria(${className} criteria)
        throws DaoException;
    
    /**
     * 根据主键id删除数据
     * 
     * @param id 主键
     * @return
     * @throws DaoException
     */
    int deleteById(Long id)
        throws DaoException;
    
    /**
     * 根据主键id列表删除数据
     * 
     * @param ids 主键列表
     * @return
     * @throws DaoException
     */
    int deleteById(Long[] ids)
        throws DaoException;
    
    /**
     * 根据主键id列表删除数据
     * 
     * @param ids 主键列表
     * @return
     * @throws DaoException
     */
    int deleteById(List<Long> ids)
        throws DaoException;
    
    /**
     * 增加记录(插入全字段)
     * 
     * @param bean 待插入对象
     * @return
     * @throws DaoException
     */
    int insert(${className} bean)
        throws DaoException;
    
    /**
     * 增加记录(仅插入非空字段)
     * 
     * @param bean 待插入对象
     * @return
     * @throws DaoException
     */
    int insertSelective(${className} bean)
        throws DaoException;
    
    /**
     * 查询全部
     * 
     * @return
     * @throws DaoException
     */
    List<${className}> queryAll()
        throws DaoException;
    
    /**
     * 根据条件查询
     * 
     * @param criteria 条件对象
     * @return
     * @throws DaoException
     */
    List<${className}> queryByCriteria(${className} criteria)
        throws DaoException;
    
    /**
     * 根据id查找数据
     * 
     * @param id 主键
     * @return
     * @throws DaoException
     */
    ${className} queryById(Long id)
        throws DaoException;
    
    /**
     * 根据条件分页查询
     * 
     * @param criteria 条件对象
     * @param pageNo 页号
     * @param pageSize 页大小
     * @return
     * @throws DaoException
     */
    PaginationSupport<${className}> queryForPagination(${className} criteria, int pageNo, int pageSize)
        throws DaoException;
    
    /**
     * 根据条件查询数据条数
     * 
     * @param criteria 条件对象
     * @return
     * @throws DaoException
     */
    public long queryTotal(${className} criteria)
        throws DaoException;
    
    /**
     * 根据复杂条件更新全字段数据
     * 
     * @param bean 待更新对象
     * @param criteria 条件对象
     * @return
     * @throws DaoException
     */
    int updateByCriteria(${className} bean, ${className} criteria)
        throws DaoException;
    
    /**
     * 根据复杂条件更新非空字段数据
     * 
     * @param bean 待更新对象
     * @param criteria 条件对象
     * @return
     * @throws DaoException
     */
    int updateByCriteriaSelective(${className} bean, ${className} criteria)
        throws DaoException;
    
    /**
     * 根据id更新全部数据
     * 
     * @param bean 待更新对象
     * @return
     * @throws DaoException
     */
    int updateById(${className} bean)
        throws DaoException;
    
    /**
     * 根据id更新非空字段数据
     * 
     * @param bean 待更新对象
     * @return
     * @throws DaoException
     */
    int updateByIdSelective(${className} bean)
        throws DaoException;
    
}