package ${packageName}.core;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import ${packageName}.common.DaoException;
import ${packageName}.common.PaginationSupport;

/**
 * 
 * BaseDAO
 * 
 * @author 00fly
 * @version [版本号, ${date?date}]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
@SuppressWarnings({"rawtypes", "hiding"})
public abstract class BaseDAO<T>
{
    private MySqlDBUtil dbUtils = MySqlDBUtil.getInstance();
    
    /**
     * 批量更新
     * 
     * @param sql 需执行的sql
     * @param params 参数组
     * @return 顺序返回执行后所影响到的行数
     * @throws SQLException
     */
    public int[] batch(String sql, Object[][] params)
        throws SQLException
    {
        return dbUtils.batch(sql, params);
    }
    
    /**
     * 批量更新
     * 
     * @param sql 需执行的sql
     * @param params List参数组
     * @return 顺序返回执行后所影响到的行数
     * @throws SQLException
     */
    public int[] batch(String sql, List<Object[]> params)
        throws SQLException
    {
        return dbUtils.batch(sql, params);
    }
    
    /**
     * 带可变参数, 执行sql插入，返回新增记录的自增主键<BR>
     * 注意： 若插入的表无自增主键则返回 0，异常的话则返回 null
     * 
     * @param sql 需执行的sql
     * @param para 可变参数
     * @return
     * @throws SQLException
     */
    public Long insert(String sql, Object... para)
        throws SQLException
    {
        return dbUtils.insert(sql, para);
    }
    
    /**
     * 带可变参数查询,返回执行结果
     * 
     * @param sql 查询sql
     * @param para 可变参数
     * @return
     * @throws SQLException
     */
    public List<Map<String, Object>> query(String sql, Object... para)
        throws SQLException
    {
        return dbUtils.query(sql, para);
    }
    
    /**
     * 带可变参数查询,返回执行结果
     * 
     * @param clazz
     * @param sql 查询sql
     * @param para 可变参数
     * @return
     * @throws SQLException
     */
    public <T> List query(T clazz, String sql, Object... para)
        throws SQLException
    {
        return dbUtils.query(clazz, sql, para);
    }
    
    /**
     * 带可变参数查询,返回首条执行结果
     * 
     * @param sql 查询sql
     * @param para 可变参数
     * @return
     * @throws SQLException
     */
    public Map<String, Object> queryFirst(String sql, Object... para)
        throws SQLException
    {
        return dbUtils.queryFirst(sql, para);
    }
    
    /**
     * 带可变参数查询,返回首条执行结果
     * 
     * @param clazz
     * @param sql 查询sql
     * @param para 可变参数
     * @return
     * @throws SQLException
     */
    public <T> Object queryFirst(T clazz, String sql, Object... para)
        throws SQLException
    {
        return dbUtils.queryFirst(clazz, sql, para);
    }
    
    /**
     * 带可变参数查询，返回long类型数据
     * 
     * @param countSql 查询记录条数的sql
     * @param para 可变参数
     * @return
     * @throws SQLException
     */
    public Long queryForLong(String countSql, Object... para)
        throws SQLException
    {
        return dbUtils.queryForLong(countSql, para);
    }
    
    /**
     * 带可变参数条件的分页查询
     * 
     * @param sql 查询sql
     * @param pageNo 页号
     * @param pageSize 每页记录数
     * @param para 可变参数
     * @return
     * @throws SQLException
     */
    public PaginationSupport queryForPagination(String sql, int pageNo, int pageSize, Object... para)
        throws SQLException
    {
        return dbUtils.queryForPagination(sql, pageNo, pageSize, para);
    }
    
    /**
     * 带可变参数条件的分页查询
     * 
     * @param clazz
     * @param sql 查询sql
     * @param pageNo 页号
     * @param pageSize 每页记录数
     * @param para 可变参数
     * @return
     * @throws SQLException
     */
    public <T> PaginationSupport queryForPagination(T clazz, String sql, int pageNo, int pageSize, Object... para)
        throws SQLException
    {
        return dbUtils.queryForPagination(clazz, sql, pageNo, pageSize, para);
    }
    
    /**
     * 带可变参数, 执行sql，返回执行影响的记录条数
     * 
     * @param sql 执行的sql 语句
     * @param para 可变参数
     * @return
     * @throws SQLException
     */
    public int update(String sql, Object... para)
        throws SQLException
    {
        return dbUtils.update(sql, para);
    }
    
    /********************* end 原生SQL执行方法 *********************/
    
    /**
     * 根据条件删除数据
     * 
     * @param criteria 条件对象
     * @return
     * @throws DaoException
     */
    public abstract int deleteByCriteria(T criteria)
        throws DaoException;
    
    /**
     * 根据主键id删除数据
     * 
     * @param ids 主键列表
     * @return
     * @throws DaoException
     */
    public abstract int deleteById(Long[] ids)
        throws DaoException;
    
    /**
     * 根据主键id删除数据
     * 
     * @param ids 主键列表
     * @return
     * @throws DaoException
     */
    public abstract int deleteById(List<Long> ids)
        throws DaoException;
    
    /**
     * 根据主键id删除数据
     * 
     * @param id 主键
     * @return
     * @throws DaoException
     */
    public abstract int deleteById(Long id)
        throws DaoException;
    
    /**
     * 增加记录(插入全字段)
     * 
     * @param bean 待插入对象
     * @return
     * @throws DaoException
     */
    public abstract int insert(T bean)
        throws DaoException;
    
    /**
     * 增加记录(仅插入非空字段)
     * 
     * @param bean 待插入对象
     * @return
     * @throws DaoException
     */
    public abstract int insertSelective(T bean)
        throws DaoException;
    
    /**
     * 查询全部
     * 
     * @return
     * @throws DaoException
     */
    public abstract List<T> queryAll()
        throws DaoException;
    
    /**
     * 根据条件查询
     * 
     * @param criteria 条件对象
     * @return
     * @throws DaoException
     */
    public abstract List<T> queryByCriteria(T criteria)
        throws DaoException;
    
    /**
     * 根据id查找数据
     * 
     * @param id 主键
     * @return
     * @throws DaoException
     */
    public abstract T queryById(Long id)
        throws DaoException;
    
    /**
     * 根据条件分页查询
     * 
     * @param clazz
     * @param criteria 条件对象
     * @param pageNo 页号
     * @param pageSize 页大小
     * @return
     * @throws DaoException
     */
    public abstract PaginationSupport<T> queryForPagination(T criteria, int pageNo, int pageSize)
        throws DaoException;
    
    /**
     * 根据条件查询数据条数
     * 
     * @param criteria 条件对象
     * @return
     * @throws DaoException
     */
    public abstract long queryTotal(T criteria)
        throws DaoException;
    
    /**
     * 根据复杂条件更新全字段数据
     * 
     * @param bean 待更新对象
     * @param criteria 条件对象
     * @return
     * @throws DaoException
     */
    public abstract int updateByCriteria(T bean, T criteria)
        throws DaoException;
    
    /**
     * 根据复杂条件更新非空字段数据
     * 
     * @param bean 待更新对象
     * @param criteria 条件对象
     * @return
     * @throws DaoException
     */
    public abstract int updateByCriteriaSelective(T bean, T criteria)
        throws DaoException;
    
    /**
     * 根据id更新全部数据
     * 
     * @param bean 待更新对象
     * @return
     * @throws DaoException
     */
    public abstract int updateById(T bean)
        throws DaoException;
    
    /**
     * 根据id更新非空字段数据
     * 
     * @param bean 待更新对象
     * @return
     * @throws DaoException
     */
    public abstract int updateByIdSelective(T bean)
        throws DaoException;
    
}
