package ${pknService};

import java.util.List;

import ${packageName}.common.PaginationSupport;
import ${packageName}.common.ServiceException;
import ${pknEntity}.${className};

/**
 * 
 * ${className}Service 接口
 * 
 * @author 00fly
 * @version [版本号, ${date?date}]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
public interface ${className}Service 
{ 
    /**
     * 新增
     * 
     * @param ${instanceName}
     * @throws ServiceException
     * @see [类、类#方法、类#成员]
     */
    void insert(${className} ${instanceName})
        throws ServiceException;
    
    /**
     * 根据id删除
     * 
     * @param id
     * @throws ServiceException
     * @see [类、类#方法、类#成员]
     */
    void deleteById(Long id)
        throws ServiceException;
    
    /**
     * 根据主键id列表删除数据
     * 
     * @param ids 主键列表
     * @return
     * @throws ServiceException
     */
    public long deleteById(Long[] ids)
        throws ServiceException;
    
    /**
     * 根据主键id列表删除数据
     * 
     * @param ids 主键列表
     * @return
     * @throws ServiceException
     */
    public long deleteById(List<Long> ids)
        throws ServiceException;
    
    /**
     * 根据id更新
     * 
     * @param ${instanceName}
     * @throws ServiceException
     * @see [类、类#方法、类#成员]
     */
    void update(${className} ${instanceName})
        throws ServiceException;
    
    /**
     * 新增/根据id更新
     * 
     * @param ${instanceName}
     * @throws ServiceException
     * @see [类、类#方法、类#成员]
     */
    void saveOrUpdate(${className} ${instanceName})
        throws ServiceException;
    
    /**
     * 根据id查询
     * 
     * @param id
     * @return
     * @throws ServiceException
     * @see [类、类#方法、类#成员]
     */
    ${className} queryById(Long id)
        throws ServiceException;
    
    /**
     * 查询全部
     * 
     * @return
     * @throws ServiceException
     */
    List<${className}> queryAll()
        throws ServiceException;

    /**
     * 根据条件分页查询
     * 
     * @param criteria 条件对象
     * @param pageNo 页号
     * @param pageSize 页大小
     * @return
     * @throws DaoException
     */
    PaginationSupport<${className}> queryForPagination(${className} criteria, int pageNo, int pageSize)
        throws ServiceException;
    
    /**
     * 事务方法
     * 
     * @throws ServiceException
     * @see [类、类#方法、类#成员]
     */
    public void testTrans()
        throws ServiceException;
}
