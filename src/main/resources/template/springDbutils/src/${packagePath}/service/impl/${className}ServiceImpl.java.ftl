package ${pknServiceImpl};

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ${packageName}.common.PaginationSupport;
import ${pknDAO}.${className}DAO;
import ${pknEntity}.${className};
import ${pknService}.${className}Service;

/**
 * 
 * ${className}Service 接口实现类
 * 
 * @author 00fly
 * @version [版本号, ${date?date}]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class ${className}ServiceImpl implements ${className}Service
{
    Logger log = LoggerFactory.getLogger(getClass());
    
    @Autowired
    ${className}DAO ${instanceName}DAO;
    
    @Override
    public void insert(${className} ${instanceName})
    {
        ${instanceName}DAO.insert(${instanceName});
    }
    
    @Override
    public void deleteById(Long id)
    {
        ${instanceName}DAO.deleteById(id);
    }
    
    @Override
    public long deleteById(Long[] ids)
    {
        return ${instanceName}DAO.deleteById(ids);
    }
    
    @Override
    public long deleteById(List<Long> ids)
    {
        return ${instanceName}DAO.deleteById(ids);
    }
    
    @Override
    public void update(${className} ${instanceName})
    {
        ${instanceName}DAO.updateById(${instanceName});
    }
    
    @Override
    public void saveOrUpdate(${className} ${instanceName})
    {
        if (${instanceName}.${pk.getMethod}() != null)
        {
            ${instanceName}DAO.updateById(${instanceName});
        }
        else
        {
            ${instanceName}DAO.insert(${instanceName});
        }
    }
    
    @Override
    public ${className} queryById(Long id)
    {
        return ${instanceName}DAO.queryById(id);
    }
    
    @Override
    public List<${className}> queryAll()
    {
        List<${className}> list = ${instanceName}DAO.queryAll();
        return list;
    }
    
    /**
     * 根据条件分页查询
     * 
     * @param ${instanceName} 条件对象
     * @param pageNo 页号
     * @param pageSize 页大小
     * @return
     */
    @Override
    public PaginationSupport<${className}> queryForPagination(${className} ${instanceName}, int pageNo, int pageSize)
    {
        return ${instanceName}DAO.queryForPagination(${instanceName}, pageNo, pageSize);
    }
    
    /**
     * 事务方法
     * 
     * 
     * @see [类、类#方法、类#成员]
     */
    public void testTrans()
    {
        List<${className}> list = queryAll();
        for (${className} ${instanceName} : list)
        {
            ${instanceName}DAO.insert(${instanceName});
        }
        int i = 1 / 0; // 抛出异常
    }
}
