package ${packageName}.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import ${packageName}.entity.${className};
import ${packageName}.service.${className}Service;

/**
 * ${className}Controller
 * 
 * @author 00fly
 * @version [版本号, ${date?date}]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
@Controller
@RequestMapping("/${className?lower_case}")
public class ${className}Controller
{
    @Autowired
    ${className}Service ${instanceName}Service;
    
    @RequestMapping(value = "/view/{id}", method = RequestMethod.GET)
    @ResponseBody
    public ${className} view(@PathVariable Long id)
    {
        ${className} ${instanceName} = ${instanceName}Service.queryById(id);
        return ${instanceName};
    }
    
    @RequestMapping(value = "/list/{pageNo}", method = RequestMethod.GET)
    @ResponseBody
    public List<${className}> list(@PathVariable Integer pageNo, @RequestParam(defaultValue = "5") int pageSize)
    {
        pageSize = Math.max(pageSize, 2);
        List<${className}> list = ${instanceName}Service.queryForPagination(new ${className}(), pageNo, pageSize).getItems();
        return list;
    }
    
    @RequestMapping(value = "/all", method = RequestMethod.GET)
    @ResponseBody
    public List<${className}> all()
    {
        List<${className}> list = ${instanceName}Service.queryAll();
        return list;
    }
    
}