#springmvc_dbutils_plus

  功能运行界面
   ![dbutils_plus](https://git.oschina.net/uploads/images/2017/0505/155958_5cd96164_722815.png "优化后的界面")


  ======================================================================================

 ![swt-jface](https://git.oschina.net/uploads/images/2017/0504/163524_e6950195_722815.png "swt-jface")

此为Windows 64位系统eclipse swt—jface包，如为32位系统请自行替换打包。

![运行1](https://git.oschina.net/uploads/images/2017/0503/200442_1d8d3b80_722815.png "运行1")
![运行2](https://git.oschina.net/uploads/images/2017/0503/200502_11e98d2d_722815.png "运行2")
![运行3](https://git.oschina.net/uploads/images/2017/0503/200521_e9ab53e6_722815.png "运行3")

利用生成的代码，新建的web工程
最终的项目结构为：
![生成的代码项目结构](https://git.oschina.net/uploads/images/2017/0503/203842_babb61df_722815.png "生成的代码项目结构")

欢迎点击链接加入技术讨论群【Java 爱码少年】：https://jq.qq.com/?_wv=1027&k=4AuWuZu